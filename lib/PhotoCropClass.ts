import ICropFrame from "./interface/CropFrame.interface";
import IMousePosition from "./interface/MousePosition.interface";
import IOptions from "./interface/Options.interface";


export default class {
    private readonly resultData: ICropFrame
    private readonly options: IOptions
    private readonly onUpdate: Function

    private mousePosition: IMousePosition
    private frameAreaPositionTemp: ICropFrame

    private isResize: Boolean

    /**
     * @param onUpdateEvent - событие на изменения параметров рамки
     * @param options - настройки класса
     */
    constructor(onUpdateEvent: Function, options?: IOptions) {
        this.onUpdate = onUpdateEvent

        const defaultOptions: IOptions = {
            minFrameArea: 20,
            containerWidth: 800,
            containerHeight: 600
        }

        this.options = {...defaultOptions, ...options}
        this.isResize = false

        this.frameAreaPositionTemp = {
            top: 0,
            left: 0,
            width: 0,
            height: 0
        }

        this.mousePosition = {
            x: 0,
            y: 0
        }

        this.resultData = {
            top: 25,
            left: 25,
            width: 250,
            height: 70
        }

        this.init()
    }

    /**
     * Обновляем размеры и положение рамки
     */
    updateTempAreaData() {
        this.frameAreaPositionTemp.width = this.resultData.width
        this.frameAreaPositionTemp.height = this.resultData.height
        this.frameAreaPositionTemp.top = this.resultData.top
        this.frameAreaPositionTemp.left = this.resultData.left
    }

    /**
     * Для ресайза рамки за верхную левую точку
     */
    dragForTopLeft(event: MouseEvent) {
        const maxWidth = this.frameAreaPositionTemp.width + this.frameAreaPositionTemp.left
        const maxHeight = this.frameAreaPositionTemp.height + this.frameAreaPositionTemp.top

        let width = this.frameAreaPositionTemp.width - (event.pageX - this.mousePosition.x)
        let height = this.frameAreaPositionTemp.height - (event.pageY - this.mousePosition.y)

        let top = this.frameAreaPositionTemp.top + (event.pageY - this.mousePosition.y)
        let left = this.frameAreaPositionTemp.left + (event.pageX - this.mousePosition.x)

        if (left < 0) {
            left = 0
        }

        if (top < 0) {
            top = 0
        }

        if (width > maxWidth) {
            width = maxWidth
        }

        if (height > maxHeight) {
            height = maxHeight
        }

        if (width > this.options.minFrameArea!) {
            this.resultData.width = width
            this.resultData.left = left
        }

        if (height > this.options.minFrameArea!) {
            this.resultData.height = height
            this.resultData.top = top
        }
    }

    /**
     * Для ресайза рамки за нижную верхную точку
     */
    dragForTopRight(event: MouseEvent) {
        const maxWidth = this.options.containerWidth! - this.frameAreaPositionTemp.left
        const maxHeight = this.frameAreaPositionTemp.height + this.frameAreaPositionTemp.top

        let width = this.frameAreaPositionTemp.width + (event.pageX - this.mousePosition.x)
        let height = this.frameAreaPositionTemp.height - (event.pageY - this.mousePosition.y)

        let top = this.frameAreaPositionTemp.top + (event.pageY - this.mousePosition.y)

        if (top < 0) {
            top = 0
        }

        if (height > maxHeight) {
            height = maxHeight
        }

        if (width > maxWidth) {
            width = maxWidth
        }

        if (width > this.options.minFrameArea!) {
            this.resultData.width = width
        }

        if (height > this.options.minFrameArea!) {
            this.resultData.height = height
            this.resultData.top = top
        }
    }

    /**
     * Для ресайза рамки за нижную левую точку
     */
    dragForBottomLeft(event: MouseEvent) {
        const maxWidth = this.frameAreaPositionTemp.width + this.frameAreaPositionTemp.left
        const maxHeight = this.options.containerHeight! - this.frameAreaPositionTemp.top

        let width = this.frameAreaPositionTemp.width - (event.pageX - this.mousePosition.x)
        let height = this.frameAreaPositionTemp.height + (event.pageY - this.mousePosition.y)

        let left = this.frameAreaPositionTemp.left + (event.pageX - this.mousePosition.x)

        if (left < 0) {
            left = 0
        }

        if (width > maxWidth) {
            width = maxWidth
        }

        if (height > maxHeight) {
            height = maxHeight
        }

        if (height > this.options.minFrameArea!) {
            this.resultData.height = height
        }

        if (width > this.options.minFrameArea!) {
            this.resultData.width = width
            this.resultData.left = left
        }
    }

    /**
     * Для ресайза рамки за нижную правую точку
     */
    dragForBottomRight(event: MouseEvent) {
        const maxWidth = this.options.containerWidth! - this.frameAreaPositionTemp.left
        const maxHeight = this.options.containerHeight! - this.frameAreaPositionTemp.top

        let width = this.frameAreaPositionTemp.width + (event.pageX - this.mousePosition.x)
        let height = this.frameAreaPositionTemp.height + (event.pageY -  this.mousePosition.y)

        if (width > maxWidth) {
            width = maxWidth
        }

        if (height > maxHeight) {
            height = maxHeight
        }

        if (width > this.options.minFrameArea!) {
            this.resultData.width = width
        }

        if (height > this.options.minFrameArea!) {
            this.resultData.height = height
        }
    }

    /**
     * Метод, который отвечает за ресайз рамки. Раставляем ивенты на 4 точки по краям
     * перед этим запоминаем положение и размеры этой рамки
     */
    resizer(event: MouseEvent, windowEvent: Function) {
        this.mousePosition.x = event.pageX
        this.mousePosition.y = event.pageY

        this.isResize = true

        window.onmousemove = (event: MouseEvent) => {
            windowEvent.bind(this)(event)
            this.onUpdate(this.resultData)
        }

        window.onmouseup = () => {
            this.isResize = false
            window.onmousemove = null

            this.updateTempAreaData()
        }
    }


    /**
     * Метод, который отвечает за перемещение рамки по рабочей области
     */
    frameAreaMoveEvent (event: MouseEvent) {
        const positionLeftNew = this.mousePosition.x - event.clientX
        const positionTopNew = this.mousePosition.y - event.clientY

        this.mousePosition.x = event.clientX
        this.mousePosition.y = event.clientY


        let topPosition = this.frameAreaPositionTemp.top - positionTopNew
        let leftPosition = this.frameAreaPositionTemp.left - positionLeftNew

        if (topPosition >= this.options.containerHeight! - this.frameAreaPositionTemp.height) {
            topPosition = this.options.containerHeight! - this.frameAreaPositionTemp.height
        }

        topPosition = (topPosition < 0) ? 0 : topPosition

        if (leftPosition >= this.options.containerWidth! - this.frameAreaPositionTemp.width) {
            leftPosition = this.options.containerWidth! - this.frameAreaPositionTemp.width
        }

        leftPosition = (leftPosition < 0) ? 0 : leftPosition
        
        this.resultData.top = topPosition
        this.resultData.left = leftPosition

        this.onUpdate(this.resultData)
        this.updateTempAreaData()
    }

    /**
     * Устанавливаем ивент на рамку для ее перемещения
     */
    frameAreaEvent(event: MouseEvent) {
        if (this.isResize) {
            return
        }

        this.mousePosition.x = event.clientX
        this.mousePosition.y = event.clientY

        document.onmousemove = this.frameAreaMoveEvent.bind(this)
        document.onmouseup = () => {
            document.onmouseup = null
            document.onmousemove = null
        }
    }

    /**
     * Запуск
     */
    init() {
        this.onUpdate(this.resultData)
        this.updateTempAreaData()
    }
}