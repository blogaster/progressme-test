/**
 * Интерфейс объекта, который хранит параметры рамки
 */
export default interface ICropFrame {
    width: number,
    height: number,
    top: number,
    left: number
}