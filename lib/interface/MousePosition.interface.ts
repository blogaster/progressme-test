/**
 * Интерфейс объекта, которы хрант позицию курсора
 */
export default interface IMousePosition {
    x: number,
    y: number
}