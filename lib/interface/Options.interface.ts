/**
 * Интерфейс настройки класса
 * minFrameArea - минимальный размер стороны рамки
 * containerWidth - ширина рабочей зоны
 * containerHeight - высота рабочей зоны
 */
export default interface IOptions {
    minFrameArea?: number,
    containerWidth?: number
    containerHeight?: number
}